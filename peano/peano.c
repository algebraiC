#include <stdio.h>
#include <stdlib.h>

enum { NUM1 = 120, NUM2 = 5};

/* Data constructor names */
enum peano_const {ZERO, SUCC};

/* Actual (algebraic) sum data type */
struct peano {
        enum peano_const type;
        struct peano *data;
};

/* Zero data constructor */
struct peano *
make_zero(struct peano *num)
{
        if (num) {
                num->type = ZERO;
                num->data = 0;
        }

        return num;
}

/* Successor data constructor */
struct peano *
succ(struct peano *num)
{
        if (num) {
                struct peano *res = malloc(sizeof(struct peano));

                if (res) {
                        res->type = SUCC;
                        res->data = num;
                        num = res;
                }
        }

        return num;
}

struct peano *
add(struct peano *n, struct peano *m)
{
        /* Emulates pattern matching */
        switch (n->type) {
        case ZERO:
                return m;
        case SUCC:
                return succ(add(n->data, m));
        default:
                fprintf(stderr, "Non-exhaustive patterns in function %s\n", 
                    __func__);
                return 0;
        }
}

/* Get numerical representation */
size_t
get_rep(struct peano *num)
{
        struct peano *curr = num;
        size_t res = 0;

        while (curr->type != ZERO) {
                res++;
                curr = curr->data;
        }

        return res;
}

/* 
 * Recursive definition of get_num. 
 * NOTE: we could make it tail recursive.
 */
int
get_rep_rec(struct peano *num)
{
        switch (num->type) {
        case ZERO:
                return 0;
        case SUCC:
                return 1 + get_rep_rec(num->data);
        default:
                fprintf(stderr, "Non-exhaustive patterns in function %s\n", 
                    __func__);
                return -1;
        }
}

int
main(void)
{
        struct peano *n = malloc(sizeof(struct peano));
        if (!n) return EXIT_FAILURE;

        make_zero(n);
        for (size_t i = 0; i < NUM1; i++) {
                n = succ(n);
        }

        struct peano *m = malloc(sizeof(struct peano));
        if (!m) return EXIT_FAILURE;

        make_zero(m);
        for (size_t i = 0; i < NUM2; i++) {
                m = succ(m);
        }
        
        printf("n is %zu\n", get_rep(n));
        printf("m is %zu\n", get_rep(m));

        struct peano *res = add(n, m);
        printf("n + m is %zu\n", get_rep(res));
       
        struct peano *curr = 0;
        while ((curr = res)) {
                res = res->data;
                free(curr);
        }

        while ((curr = n)) {
                n = n->data;
                free(curr);
        }
                
        return EXIT_SUCCESS;
}

