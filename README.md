# AlgebraiC

Implementation of Peano numbers and lazy list as sum data types in C as discussed in
[https://nullbuffer.com/2019/11/15/algebraic_c.html](https://nullbuffer.com/2019/11/15/algebraic_c.html).

## Building

To compile a specific example, you can simply run

    $ make

in its folder.
